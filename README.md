# Bootstrap v4 layout for userarea of Pheix CMS

## Annotation

This repository contains pages layout for CMS Pheix.

## License information

You can redistribute this layout pack and/or modify it under the terms of the Artistic License 2.0.

## Credits

[Pheix Perl 6 brew site](https://perl6.pheix.org)

[Bootstrap v4.0](https://getbootstrap.com/docs/4.0/getting-started/download/)

[The Artistic License 2.0](https://opensource.org/licenses/Artistic-2.0)

[Feedback](http://pheix.org/feedback.html)
